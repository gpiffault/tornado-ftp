#!/usr/bin/env python3
import socket
import subprocess
import os.path

import tornado.tcpserver
import tornado.ioloop
import tornado.iostream
import tornado.concurrent
import tornado.gen


class FTPServer(tornado.tcpserver.TCPServer):

    def __init__(self, path):
        super().__init__()
        self.path = path

    def handle_stream(self, stream, address):
        tornado.ioloop.IOLoop.instance().add_future(
            ControlConnection(self, stream, address).start(),
            lambda f: f.result())


class PassiveListener(tornado.tcpserver.TCPServer):

    def __init__(self, client_ip):
        super().__init__()
        self.client_ip = client_ip
        self.stream = None
        self.bind(0, family=socket.AF_INET)
        self.start()

    def get_socket(self):
        return list(self._sockets.values())[0]

    def format_host(self):
        addr = socket.gethostbyname(socket.gethostname())
        port = self.get_socket().getsockname()[1]
        result = addr.replace(".", ",")
        result += "," + str(port // 256)
        result += "," + str(port % 256)
        return "(" + result + ")"

    def handle_stream(self, stream, addr):
        print("New data connection")
        if addr[0] != self.client_ip:
            print("client ip doesn't match, closing connection")
            stream.close()
        self.stream = stream
        tornado.ioloop.IOLoop.instance().add_callback(self.stop)

    @tornado.gen.coroutine
    def send(self, data):
        self.stream.write(data)
        self.stream.close()
        print("Data sent, closing data connection")


class ControlConnection:

    def __init__(self, server, stream, address):
        self.server = server
        self.stream = stream
        self.address = address
        self.encoding = "ascii"
        self.start_position = 0

    @tornado.gen.coroutine
    def writeline(self, value):
        value += "\r\n"
        yield self.stream.write(value.encode(self.encoding))

    @tornado.gen.coroutine
    def readline(self):
        value = yield self.stream.read_until(b"\r\n")
        value = value.decode(self.encoding)
        value = value.rstrip("\r\n")
        return value

    @tornado.gen.coroutine
    def start(self):
        print("Incoming connection from {}".format(self.address))
        yield self.writeline("220")
        self.running = True
        while self.running:
            try:
                yield self.handle_command()
            except tornado.iostream.StreamClosedError:
                self.close()

    def close(self):
        print("Closing connection from {}".format(self.address))
        self.running = False
        self.stream.close()

    @tornado.gen.coroutine
    def handle_command(self):
        command = yield self.readline()
        print("Received command: " + command)
        command = command.split(" ", 1)
        if len(command) == 1:
            command = command[0]
            parameters = ""
        else:
            command, parameters = command

        if command == "USER":
            yield self.writeline("230")

        elif command == "SYST":
            yield self.writeline("215 UNIX Type: L8")

        elif command == "FEAT":
            yield self.writeline("211-")
            yield self.writeline(" PASV")
            yield self.writeline(" REST")
            yield self.writeline("211 ")

        elif command == "PWD":
            yield self.writeline('257 "/"')

        elif command == "CWD":
            yield self.writeline('250')

        elif command == "TYPE":
            yield self.writeline('200')

        elif command == "PASV":
            self.data_connection = PassiveListener(self.address[0])
            yield self.writeline("227 " + self.data_connection.format_host())

        elif command == "LIST":
            yield self.writeline("150")
            yield self.data_connection.send(
                subprocess.check_output(["ls", "-l", self.server.path]))
            yield self.writeline("226")

        elif command == "RETR":
            yield self.writeline("150")
            filename = os.path.basename(parameters)
            # Wait for opened data connection ?
            fh = open(os.path.join(self.server.path, filename), "rb")
            fh.seek(self.start_position)
            yield self.data_connection.send(fh.read())
            self.start_position = 0
            yield self.writeline("226")

        elif command == "REST":
            self.start_position = int(parameters)
            yield self.writeline("350")

        elif command == "QUIT":
            yield self.writeline("221")
            self.close()

        else:
            yield self.writeline("502")


if __name__ == '__main__':
    PORT = 2121
    server = FTPServer(".")
    server.listen(PORT)
    print("Starting server on port {}".format(PORT))
    tornado.ioloop.IOLoop.instance().start()
